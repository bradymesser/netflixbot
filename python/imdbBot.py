#https://www.rottentomatoes.com/top/bestofrt/top_100_horror_movies/
#https://www.justwatch.com/us/provider/netflix
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.common.exceptions import NoSuchElementException
import time

def isInt(s):
    try:
        int(s)
        return True
    except ValueError:
        return False

def changeWord(word):
    for letter in word:
        if letter == "_" or letter == "-":
            word = word.replace(letter," ")
        if isInt(letter):
            word = word.replace(letter,"")
    return word.lstrip()

pages = 10
numPerPage = 50
driver = webdriver.Safari()
try:
    movieList = list()
    ratingDict = dict()
    k = 0
    #driver = webdriver.Safari()
    driver.get("https://www.imdb.com/search/title?genres=horror&sort=user_rating,desc&title_type=feature&num_votes=25000,&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=5aab685f-35eb-40f3-95f7-c53f09d542c3&pf_rd_r=P6VD4P4CDDFVJEYXDNFJ&pf_rd_s=right-6&pf_rd_t=15506&pf_rd_i=top&ref_=chttp_gnr_12")
    for j in range (1,pages):
        for i in range(1,numPerPage):
            time.sleep(.5)
            element  = '//*[@id="main"]/div/div[3]/div/div[' + str(i) + ']/div[3]/h3/a'
            year = '//*[@id="main"]/div/div[3]/div/div[' + str(i) + ']/div[3]/h3/span[2]'
            rating = '//*[@id="main"]/div/div[3]/div/div[' + str(i) + ']/div[3]/div/div[1]/strong'

            var = (driver.find_element_by_xpath(element).get_attribute("textContent"))
            yearPub = (driver.find_element_by_xpath(year).get_attribute("textContent"))
            rate = (driver.find_element_by_xpath(rating).get_attribute("textContent"))
            movieList.append(var + " " + yearPub)
            k = k + 1
            ratingDict[var + " " +  yearPub] = rate
            #print(movieList[i-1] + ratingDict[movieList[i-1]])
            #print('\n')
        if j == 1:
            driver.find_element_by_xpath('//*[@id="main"]/div/div[4]/a').send_keys(Keys.RETURN)
        else:
            driver.find_element_by_xpath('//*[@id="main"]/div/div[4]/a[2]').send_keys(Keys.RETURN)
    driver.get("https://www.justwatch.com/us/provider/netflix")
    for i in range(1,pages * numPerPage):
        #driver.get("https://www.justwatch.com/us/provider/netflix")
        #print("SEARCHING FOR: " + movieList[i-1] )
        try:
            driver.find_element_by_xpath('/html/body/div[2]/div[2]/div/div[2]/input[1]').clear()
            driver.find_element_by_xpath('/html/body/div[2]/div[2]/div/div[2]/input[1]').send_keys(movieList[i-1])
            driver.find_element_by_xpath('/html/body/div[2]/div[2]/div/div[2]/input[1]').send_keys(Keys.RETURN)
        except NoSuchElementException:
            time.sleep(2)
            driver.find_element_by_xpath('/html/body/div[2]/div[2]/div/div[2]/input[1]').clear()
            driver.find_element_by_xpath('/html/body/div[2]/div[2]/div/div[2]/input[1]').send_keys(movieList[i-1])
            driver.find_element_by_xpath('/html/body/div[2]/div[2]/div/div[2]/input[1]').send_keys(Keys.RETURN)
        try:
            time.sleep(1.5)
            #var = driver.find_element_by_xpath('/html/body/div[3]/filter-bar/ng-transclude/core-list/div/div/div[1]/search-result-entry/div/div[2]/div[1]/a/span[1]').get_attribute("textContent")
            var = driver.find_element_by_xpath('/html/body/div[3]/filter-bar/ng-transclude/core-list/div/div/div[1]/search-result-entry/div/div[2]/div[2]/div/div[1]/div/div/div/div/div[2]/div[1]/div[2]/div[1]/div/a/div/img').get_attribute("alt")
            if var == "Netflix":
                print(movieList[i-1].encode('utf-8') + ratingDict[movieList[i-1]].encode('utf-8'))
        except NoSuchElementException:
            time.sleep(.1)
except KeyboardInterrupt:
    driver.close()

driver.close()

#/html/body/div[3]/filter-bar/ng-transclude/core-list/div/div/div[1]/search-result-entry/div/div[2]/div[2]/div/div[1]/div/div/div/div/div[2]/div[1]/div[2]/div[1]/div/a/div/img
