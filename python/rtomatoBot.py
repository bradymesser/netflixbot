#https://www.rottentomatoes.com/top/bestofrt/top_100_horror_movies/
#https://www.justwatch.com/us/provider/netflix
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.common.exceptions import NoSuchElementException
import time

def isInt(s):
    try:
        int(s)
        return True
    except ValueError:
        return False

def changeWord(word):
    for letter in word:
        if letter == "_" or letter == "-":
            word = word.replace(letter," ")
        if isInt(letter):
            word = word.replace(letter,"")
    return word.lstrip()


driver = webdriver.Safari()
try:
    movieList = list()
    ratingDict = dict()
    #driver = webdriver.Safari()
    driver.get("https://www.rottentomatoes.com/top/bestofrt/top_100_horror_movies/")
    for i in range(1,100):
        element  = '//*[@id="top_movies_main"]/div/table/tbody/tr[' + str(i) + ']/td[3]/a'
        rating = '//*[@id="top_movies_main"]/div/table/tbody/tr[' + str(i) +']/td[2]/span/span[2]'
        var = (driver.find_element_by_xpath(element).get_attribute("textContent"))
        rate = (driver.find_element_by_xpath(rating).get_attribute("textContent"))
        movieList.append(var.lstrip())
        ratingDict[var.lstrip()] = rate
        #print(movieList[i-1] + ratingDict[movieList[i-1]])
        #print('\n')
    driver.get("https://www.justwatch.com/us/provider/netflix")
    for i in range(1,100):
        #driver.get("https://www.justwatch.com/us/provider/netflix")
        #print("SEARCHING FOR: " + movieList[i-1] )
        try:
            driver.find_element_by_xpath('/html/body/div[2]/div[2]/div/div[2]/input[1]').clear()
            driver.find_element_by_xpath('/html/body/div[2]/div[2]/div/div[2]/input[1]').send_keys(movieList[i-1])
            driver.find_element_by_xpath('/html/body/div[2]/div[2]/div/div[2]/input[1]').send_keys(Keys.RETURN)
        except NoSuchElementException:
            time.sleep(2)
            driver.find_element_by_xpath('/html/body/div[2]/div[2]/div/div[2]/input[1]').clear()
            driver.find_element_by_xpath('/html/body/div[2]/div[2]/div/div[2]/input[1]').send_keys(movieList[i-1])
            driver.find_element_by_xpath('/html/body/div[2]/div[2]/div/div[2]/input[1]').send_keys(Keys.RETURN)
        try:
            time.sleep(1.5)
            #var = driver.find_element_by_xpath('/html/body/div[3]/filter-bar/ng-transclude/core-list/div/div/div[1]/search-result-entry/div/div[2]/div[1]/a/span[1]').get_attribute("textContent")
            var = driver.find_element_by_xpath('/html/body/div[3]/filter-bar/ng-transclude/core-list/div/div/div[1]/search-result-entry/div/div[2]/div[2]/div/div[1]/div/div/div/div/div[2]/div[1]/div[2]/div[1]/div/a/div/img').get_attribute("alt")
            if var == "Netflix":
                print(movieList[i-1].encode('utf-8') + ratingDict[movieList[i-1]].encode('utf-8'))
        except NoSuchElementException:
            time.sleep(.1)
except KeyboardInterrupt:
    driver.close()

driver.close()

#/html/body/div[3]/filter-bar/ng-transclude/core-list/div/div/div[1]/search-result-entry/div/div[2]/div[2]/div/div[1]/div/div/div/div/div[2]/div[1]/div[2]/div[1]/div/a/div/img
