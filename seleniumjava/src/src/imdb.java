import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.htmlunit.HtmlUnitDriver;
import java.io.PrintWriter;
import java.util.*;

public class imdb {
    public static void main(String[] args) {
        WebDriver driver = new HtmlUnitDriver();
        PrintWriter writer;
        try {
            writer = new PrintWriter("horror.txt", "UTF-8");
        }
        catch (Exception e) {
            System.out.println("Could not instantiate writer.");
            return;
        }
        int pages = 10;
        int numPerPage = 50;
        String elementToFind;
        String ratingToFind;
        //String year;
        String mov;
        String var;
        String temp;
        WebElement element;
        WebElement yr;
        WebElement rate;
        List movieList = new ArrayList();
        Map<String, String> dictionary = new HashMap<String, String>();

        // Navigate to imdb
        driver.get("https://www.imdb.com/search/title?genres=horror&sort=user_rating,desc&title_type=feature&num_votes=25000,&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=5aab685f-35eb-40f3-95f7-c53f09d542c3&pf_rd_r=P6VD4P4CDDFVJEYXDNFJ&pf_rd_s=right-6&pf_rd_t=15506&pf_rd_i=top&ref_=chttp_gnr_12");
        for (int i = 1; i <= pages; i++) {
            for (int j = 1; j <= numPerPage; j++) {
                elementToFind  = "//*[@id='main']/div/div[3]/div/div[" + Integer.toString(j) + "]/div[3]/h3/a";
                //year = "//*[@id='main']/div/div[3]/div/div[" + Integer.toString(j) + "]/div[3]/h3/span[2]";
                ratingToFind = "//*[@id='main']/div/div[3]/div/div[" + Integer.toString(j) + "]/div[3]/div/div[1]/strong";
                element = driver.findElement(By.xpath(elementToFind));
                //yr = driver.findElement(By.xpath(year));
                rate = driver.findElement(By.xpath(ratingToFind));
                mov = element.getText();// + " " + yr.getText();
                movieList.add(mov);
                dictionary.put(mov,rate.getText());
                System.out.println("Added movie: " + mov);
            }
            //go to next page
            element = driver.findElement(By.cssSelector("html body#styleguide-v2.fixed div#wrapper div#root.redesign div#pagecontent.pagecontent div#content-2-wide.redesign div#main div.article div.nav div.desc a.lister-page-next.next-page"));
            element.click();
        }
        driver.get("https://www.finder.com/netflix-movies");
        driver.findElement(By.xpath("/html/body/div[1]/div/div/div[2]/div[2]/div[2]/div[2]/button")).click();
        for (int i = 0; i < numPerPage * pages; i++) {
            var = "";
            try {
                var = driver.findElement(By.xpath("//*[contains(text(),'" + movieList.get(i) + "')]")).getText();
            }
            catch (Exception e) {

            }
            if (var.equals(movieList.get(i))) {
                System.out.println("Found: " + movieList.get(i).toString() + " " + dictionary.get(movieList.get(i)));
                temp = "Found: " + movieList.get(i).toString() + " " + dictionary.get(movieList.get(i));
                writer.println(temp);
            }
        }
        writer.close();
        driver.quit();
    }
}